import React, { Component } from 'react';

import './App.css';
import { Switch, Route, Router} from 'react-router-dom'
import { Row } from 'antd'
import Main from './components/Main';

import history from './history';

class App extends Component{
  
  render(){

    return(
      <Row>
        <Router history={history}>
        <Switch>
          <Route component={Main} path="/" exact={true}/>

        </Switch>
        </Router>
      </Row>
    )
  }
}

export default App;
