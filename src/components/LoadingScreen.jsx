import React from 'react';
import { Spin }  from "antd";

const LoadingScreen = props => (
    <div className="loading-screen">
        <Spin/>
    </div>
)

export default LoadingScreen;