import React, { Component } from 'react';
import { Row, Form, Input, Col, Checkbox, Icon, Button, message } from "antd";

import 'antd/dist/antd.css'
import Axios from 'axios';
class CustomForm extends Component {
    state = {
        loading: false,
        phone: "",
        firstName: "",
        email: "",
        products: "",
        interests: {
            SOCIALPAY: false,
            INTERNETBANK: false,
            POS: false,
            CHATBOT: false,
            COLORCARD: false,
            SAVING: false,
            OTHER: false
        }
    }

    componentDidMount() {
        const { data } = this.props;
        let first_name_field = data.find(_ =>
            _.id === "first_name"
        )
        let phone_field = data.find(_ =>
            _.id === "phone"
        )
        let email_field = data.find(_ =>
            _.id === "email"
        )
        let products_field = data.find(_ =>
            _.id === "products"
        )
        let other_field = data.find(_ =>
            _.id === "products_other"
        )

        // let lastName = last_name_field.text.length > 0 ? last_name_field.text[0] : ""
        let firstName = first_name_field && first_name_field.text.length > 0 ? first_name_field.text[0] : ""
        let email = email_field && email_field.text.length > 0 ? email_field.text[0] : ""
        let phone = phone_field && phone_field.text.length > 0 ? phone_field.text[0] : ""
        let products = products_field && products_field.text.length > 0 ? products_field.text[0] : ""
        let other = other_field && other_field.text.length > 0 ? other_field.text[0] : ""
        products = products.replace(/" "/g, "").split(", ")
        firstName = firstName.toLowerCase();
        firstName = firstName[0].toUpperCase() + firstName.substr(1, firstName.length);
        console.log("%c" + other, "font-size: 40px; color:red")
        this.setState({
            phone: phone,
            email: email.toLowerCase(),
            firstName: firstName,
            interests: {
                SOCIALPAY: products.indexOf("SOCIALPAY") >= 0,
                INTERNETBANK: products.indexOf("INTERNETBANK") >= 0,
                POS: products.indexOf("POS") >= 0,
                SAVING: products.indexOf("SAVING") >= 0,
                COLORCARD: products.indexOf("COLORCARD") >= 0,
                OTHER: products.indexOf("OTHER") >= 0,

            },
            other: other
        }, () => { console.log(this.state) })
    }

    handleInput = e => {
        let val = e.target.value;
        let name = e.target.name;

        this.setState({
            [name]: val
        })
    }

    handleCheck = e => {
        let name = e.target.name;


        this.setState(prevState => ({
            interests: {
                ...prevState.interests,
                [name]: !prevState.interests[name],
            }
        }), () => {
            console.log(name, this.state)
        })
    }

    onSubmit = () => {
        this.setState({
            loading: true
        }, () => {
            const { interests, email, phone, firstName, other } = this.state;
            let formdata = new FormData();
            let products = ""
            Object.keys(interests).forEach(key => {
                if (interests[key])
                    products = products + key + ","
            })

            if (other !== "") {
                products = products + "OTHER:" + other;
            }
            // console.log(othe
            formdata.set("email", email)
            formdata.set("first_name", firstName)
            formdata.set("phone", phone)
            formdata.set("products", products)      



            Axios.post("https://ocr.golomtbank.com/product/v2/lead", formdata).then(response => {
                // message.success("Амжилттай хадгаллаа", 2);
                this.setState({
                    loading: false
                })
                this.props.onSuccess();
            }).catch(err => {
                this.setState({
                    loading: false
                })
                message.warn("Алдаа гарлаа, дахин оролдоно уу", 2)
            })

        })
    }

    render() {
        const { loading, firstName, phone, email, interests } = this.state;
        return (
            <Row style={{ padding: "25px" }}>
                <Col style={{ display: "flex", justifyContent: "right", width: "100%" }}>
                    <Button type="default" onClick={this.props.onBack}><Icon type="left-circle" theme="twoTone" /></Button>
                </Col>
                <h4 style={{ marginTop: "25px" }}>
                    Холбоо барих мэдээлэл
                </h4>
                <Form layout="vertical">
                    <Form.Item label="Нэр">
                        <Input placeholder="Нэр" type="text" value={firstName} name="firstName" onInput={e => this.handleInput(e)} defaultValue={firstName} />
                    </Form.Item>
                    <Form.Item label="Утас">
                        <Input type="text" placeholder="Утас" defaultValue={phone} value={phone} name="phone" onInput={e => this.handleInput(e)} />
                    </Form.Item>
                    <Form.Item label="И-Мэйл">
                        <Input type="text" placeholder="И-Мэйл" defaultValue={email} name="email" value={email} onInput={e => this.handleInput(e)} />
                    </Form.Item>
                </Form>
                <h4>
                    Сонирхож буй бүтээгдэхүүн үйлчилгээ
                </h4>
                <Col>
                    <Col><Checkbox name="SOCIALPAY" onClick={e => this.handleCheck(e)} checked={interests.SOCIALPAY}>Social Pay</Checkbox></Col>
                    <Col><Checkbox name="INTERNETBANK" onClick={e => this.handleCheck(e)} checked={interests.INTERNETBANK}>Интернет банк/Смарт банк</Checkbox></Col>
                    <Col><Checkbox name="COLORCARD" onClick={e => this.handleCheck(e)} checked={interests.COLORCARD}>"Color" карт/ Бусад карт</Checkbox></Col>
                    <Col><Checkbox name="POS" onClick={e => this.handleCheck(e)} checked={interests.POS}>Пос төхөөрөмж Интернет худалдаа</Checkbox></Col>
                    <Col><Checkbox name="SAVING" onClick={e => this.handleCheck(e)} checked={interests.SAVING}>Харицлах Хадгаламж</Checkbox></Col >
                    <Col><Checkbox name="OTHER" onClick={e => this.handleCheck(e)} checked={interests.OTHER}>Бусад</Checkbox></Col>
                    {/* <Input /> */}
                </Col>
                <Col style={{ marginTop: "25px", display: "flex", justifyContent: "center" }}>
                    <button onClick={this.onSubmit}><Icon type={loading ? "loading" : "file-done"}></Icon> Хүсэлт илгээх</button>
                </Col>
            </Row>
        )
    }
}

export default CustomForm;