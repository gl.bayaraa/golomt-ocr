import React, { Fragment, Component } from 'react';
import { Col, Icon, message } from "antd"
import main from '../assets/main.jpg'
import logo from '../assets/logo.png'
import { ReactComponent as Icon1 } from '../assets/1.svg'
import { ReactComponent as Icon2 } from '../assets/2.svg'
import { ReactComponent as Icon3 } from '../assets/3.svg'
import { ReactComponent as Icon4 } from '../assets/4.svg'
import axios from 'axios';
import 'antd/dist/antd.css'
import CustomForm from './CustomForm';
import LoadingScreen from './LoadingScreen';
class Main extends Component {
    inputRef = null;
    state = {
        loading: false,
        file: null,
        data: {},
        page: "main"
    }

    handleFileChange = event => {
        this.setState({
            loading: true,
            file: event.target.files[0]
        }, () => {
            let formData = new FormData();
            formData.set("type", "DEMO")
            formData.set("form", this.state.file)
            axios.post("https://ocr.golomtbank.com/ocr/v1/recognise/", formData, { headers: { "Content-Type": "multipart/form-data" } }).then(response => {
                console.log(response)
                this.setState({
                    loading: false,
                    page: "form",
                    data: response.data.fields[0]
                }, () => {
                    message.success("Амжилттай.", 2)
                    // history.push("/form", {data: response.data})
                })

            }).catch(err => {
                message.error("Алдаа гарлаа, интернет холболтоо шалгана уу", 2)
                this.setState({
                    loading: false
                })
            })
        })
    }
    handleClick = () => {
        this.inputRef.click();
    }
    render() {
        const { loading, page, data } = this.state;
        return (
            <Fragment>
                <Col className="logo">
                    <img src={logo} alt="logo" />
                </Col>
                {
                    page === "main" ?
                        <Fragment>
                            {loading ? <LoadingScreen /> : null}
                            <Col className="image">
                                <img src={main} alt="main" className="main-image" />
                            </Col>
                            <Col>
                                <p style={{ textAlign: "center", fontSize: "0.7rem", }}>
                                    <b>Голомт Банкны</b> үйлчилгээг сонирхсонд баярлалаа.<br />
                                    Та бөглөсөн хуудасны зургийг оруулна уу.<br />
                                    <Icon type="arrow-down" className="arrow-down" />
                                </p>
                            </Col>
                            <Col style={{
                                display: "flex",
                                justifyContent: "center"
                            }}>

                            </Col>
                            <Col style={{
                                display: "flex",
                                justifyContent: "center"
                            }}>
                                <button onClick={this.handleClick} disabled={loading ? true : false}>
                                    <Icon type={loading ? "loading" : "upload"}></Icon>  Зураг оруулах
                </button>
                                <input type="file" accept="image/*" ref={ref => this.inputRef = ref} onChange={e => this.handleFileChange(e)} style={{ display: "none" }} />
                            </Col>
                            <Col className="bottom">
                                <Col className="icons">

                                    <Icon1 />
                                    <Icon2 />
                                    <Icon3 />
                                    <Icon4 />

                                </Col>
                                <Col className="copyright">
                                    &copy; Golomt Bank, All rights reserved
                </Col>
                            </Col>
                        </Fragment> :
                        (

                            page === "success" ?
                                <Fragment>

                                    <Col className="success-screen">
                                        <p> <Icon type="check" /></p>
                                        <p style={{ textAlign: "center", width: "100%" }}>Хүсэлт амжилттай илгээгдлээ.</p>
                                        <Col style={{ display: "flex", justifyContent: "center", paddingTop: "100px" }}>
                                            <button onClick={() => {
                                                this.setState({
                                                    page: "main"
                                                })
                                            }}><Icon type="check" /> Болсон</button>
                                        </Col>
                                    </Col>

                                </Fragment>
                                : <CustomForm onBack={() => {
                                    this.setState({
                                        page: "main"
                                    })
                                }} data={data}
                                    onSuccess={() => {
                                        this.setState({
                                            page: "success"
                                        })
                                    }}
                                />

                        )
                }

            </Fragment>
        )
    }
}

export default Main;